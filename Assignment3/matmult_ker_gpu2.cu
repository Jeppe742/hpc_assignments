__global__ void matmult_ker_gpu2(int m, int n, int k, double *A,double *B, double *C){
    int i,j,l;
    /*Remember initializing*/
    i=blockIdx.y*blockDim.y+threadIdx.y;
    j=blockIdx.x*blockDim.x+threadIdx.x;
    if(i<m || j<n){
        for(l=0;l<k;l++)
                C[i*n+j] += A[i*k+l] * B[l*n+j];
                }
}
}