#include <cublas_v2.h>
extern "C"{
#include <math.h>
#include "cblas.h"
#include "libmatmult.h"
#include <stdio.h>
#include "omp.h"
#define min(a,b) ( (a) < (b) ? (a) : (b))
#define BLOCK_SIZE 16
void init_mat(int m, int n, double *C){
    int i, j;
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            C[i*n+j] = 0.0;
        }
    }
}
__global__ void matmult_ker_gpu1(int m, int n, int k, double *A,double *B, double *C){
    int i,j,l;
    for(i=0;i<m;i++){
        for(l=0;l<k;l++){
            for(j=0;j<n;j++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
}
void matmult_gpu1(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */
    matmult_ker_gpu1<<<1,1>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}
__global__ void matmult_ker_gpu2(int m, int n, int k, double *A,double *B, double *C){
    int i,j,l;
    i=blockIdx.y*blockDim.y+threadIdx.y;
    j=blockIdx.x*blockDim.x+threadIdx.x;
    if(i<m && j<n){
        double tmp=0.0;
        for(l=0;l<k;l++){
            tmp+=A[i*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp;
       }       
}
void matmult_gpu2(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    int blocksize = 32;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */
    dim3 gridDim(n/blocksize+1,m/blocksize+1);
    dim3 blockDim(blocksize,blocksize);
    /*printf("%i %i\n",gridDim.x,gridDim.y);*/
    matmult_ker_gpu2<<<gridDim,blockDim>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}

__global__ void matmult_ker_gpu3(int m, int n, int k, double *A, double *B, double *C){
    int i,j,l;
    i=blockIdx.y*blockDim.y+threadIdx.y;
    j=blockIdx.x*blockDim.x+threadIdx.x;
    i*=2;

    // i is a multiple of two
    if(i<(m-1) && j<n){
        double tmp [2] = {0.0};

        for(l=0;l<k;l++){
            tmp[0] +=A[i*k+l] * B[l*n+j];
            tmp[1] +=A[(i+1)*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp[0];
        C[(i+1)*n+j]=tmp[1];
    }
    // only one row left
    else if(i<m && j<n){
        double tmp = 0.0;
        for(l=0;l<k;l++){
            tmp+=A[i*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp;
    }
}
void matmult_gpu3(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    int blocksize=16;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */
    dim3 gridDim(n/blocksize+1,m/(blocksize*2)+1);
    dim3 blockDim(blocksize,blocksize);
    matmult_ker_gpu3<<<gridDim,blockDim>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}


__global__ void matmult_ker_gpu4(int m, int n, int k, double *A, double *B, double *C){
    int i,j,l;
    i=blockIdx.y*blockDim.y+threadIdx.y;
    j=blockIdx.x*blockDim.x+threadIdx.x;

    i*=3;
    // i is a multiple of three
    if(i<(m-2) && j<n){
        double tmp [3] = {0.0};

        for(l=0;l<k;l++){
            tmp[0] +=A[i*k+l] * B[l*n+j];
            tmp[1] +=A[(i+1)*k+l] * B[l*n+j];
            tmp[2] +=A[(i+2)*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp[0];
        C[(i+1)*n+j]=tmp[1];
        C[(i+2)*n+j]=tmp[2];
    }
    // i is a multiple of two
    if(i<(m-1) && j<n){
        double tmp [2] = {0.0};

        for(l=0;l<k;l++){
            tmp[0] +=A[i*k+l] * B[l*n+j];
            tmp[1] +=A[(i+1)*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp[0];
        C[(i+1)*n+j]=tmp[1];
    }
    // only one row left
    else if(i<m && j<n){
        double tmp = 0.0;
        for(l=0;l<k;l++){
            tmp+=A[i*k+l] * B[l*n+j];
        }
        C[i*n+j]=tmp;
    }
 
}
void matmult_gpu4(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    int blocksize=16;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */
    dim3 gridDim(n/blocksize+1,m/(blocksize*3)+1);
    dim3 blockDim(blocksize,blocksize);
    matmult_ker_gpu4<<<gridDim,blockDim>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}

void matmult_gpulib(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    const double alph=1.0;
    const double bet=0.0;
    const double *alpha=&alph;
    const double *beta=&bet;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    cublasHandle_t cuHandle;
    cublasCreate(&cuHandle);
    cublasDgemm(cuHandle, CUBLAS_OP_N,CUBLAS_OP_N,m,n,k,alpha,A_d,m,B_d,k,beta,C_d,m);
    cublasDestroy(cuHandle);
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}
__global__ void matmul_ker_gpu5(int m,int n,int k,double *A,double *B,double *C){

    int A_width = k;

    // Block row and column
    int blockRow = blockIdx.y;
    int blockCol = blockIdx.x;

    double *Csub = &C[n*BLOCK_SIZE*blockRow+BLOCK_SIZE*blockCol];

    double cvalue = 0.0;

    int row = threadIdx.y;
    int col = threadIdx.x;

    __shared__ double As[BLOCK_SIZE][BLOCK_SIZE];
    __shared__ double Bs[BLOCK_SIZE][BLOCK_SIZE];


    for (int m=0; m < (A_width / BLOCK_SIZE); ++m){

        // Get sub of A and B
        double *Asub = &A[k*BLOCK_SIZE*blockRow + BLOCK_SIZE*m];
        double *Bsub = &B[n*BLOCK_SIZE*m + BLOCK_SIZE*blockCol];
        As[row][col]=Asub[row*k+col];
        Bs[row][col]=Bsub[row*n+col];
        __syncthreads();

        for (int e=0; e<BLOCK_SIZE; ++e)
            cvalue += As[row][e] * Bs[e][col];

    __syncthreads();


    }
    Csub[n * row + col] = cvalue;

}
/*__global__ void matmult_ker_gpu5(int m,int n,int k,double *A,double *B,double *C){
    
int a,b;
    // 
    int i = blockIdx.y;
    //
    int j = blockIdx.x;
    //
    int l = threadIdx.y; 
    int g = threadIdx.x;
    //Csub=C[n*bs*i+bs*j]
    double *Csub=&C[n*BLOCK_SIZE*i+BLOCK_SIZE*j];
    double tmp = 0.0;
    for(a=0; a < k/BLOCK_SIZE; a++){
        double *Asub=&A[k*BLOCK_SIZE*i+BLOCK_SIZE*a];
        double *Bsub=&B[n*BLOCK_SIZE*a+j*BLOCK_SIZE];
        __shared__ double as[BLOCK_SIZE*BLOCK_SIZE];
        __shared__ double bs[BLOCK_SIZE*BLOCK_SIZE];
        as[l*BLOCK_SIZE+g]=Asub[l*k+g];
        bs[l*BLOCK_SIZE+g]=Bsub[l*n+g];

        __syncthreads();
        
        for(b=0; b < BLOCK_SIZE; b++){

            tmp += as[l*BLOCK_SIZE + b] * bs[b*BLOCK_SIZE + g];

        }
        __syncthreads();
        }
        C[n*BLOCK_SIZE*i+BLOCK_SIZE*j] = tmp;
}
*/
void matmult_gpu5(int m, int n, int k, double *A, double *B, double *C){
   double *A_d, *B_d, *C_d;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */

    dim3 gridDim(n/BLOCK_SIZE,m/BLOCK_SIZE);
    dim3 blockDim(BLOCK_SIZE,BLOCK_SIZE);
    matmul_block<<<gridDim,blockDim>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}
void matmult_nat(int m,int n,int k,double *A,double *B,double *C){
    int i,j,l;
    init_mat(m,n,C);

    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            for(l=0;l<k;l++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}

void matmult_lib(int m,int n,int k,double *A,double *B,double *C){

    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,m,n,k,1.0,A, k,B,n,0,C,n);
}
void matmult_mnk(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            for(l=0;l<k;l++){
                C[i*n+j] += A[i*k+l]* B[l*n+j];
            }
        }
    }
}
void matmult_mkn(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
    for(i=0;i<m;i++){
        for(l=0;l<k;l++){
            for(j=0;j<n;j++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
void matmult_kmn(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
        for(l=0;l<k;l++){
        for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
void matmult_knm(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
    for(l=0;l<k;l++){
        for(j=0;j<n;j++){
            for(i=0;i<m;i++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
void matmult_nkm(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
    for(j=0;j<n;j++){
        for(l=0;l<k;l++){
            for(i=0;i<m;i++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
void matmult_nmk(int m,int n,int k,double *A,double *B,double *C){    
    int i,j,l;
    init_mat(m,n,C);
    for(j=0;j<n;j++){
        for(i=0;i<m;i++){
            for(l=0;l<k;l++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
 /*void matmult_blk(int m,int n,int k,double **A,double **B,double **C, int bs){

     int i,j,l,io,jo,lo;
     int mini, minj, minl;
     init_mat(m,n,C);
    
    for(io=0;io<m;io+=bs){
         mini = min(m-io,bs)+io;
         for(lo=0;lo<k;lo+=bs){
            minl = min(k-lo,bs)+lo;
             for(jo=0;jo<n;jo+=bs){
                 minj = min(n-jo,bs)+jo;
                 for(i=io;i<mini;i++){
                    for(l=lo;l<minl;l++){
                         for(j=jo;j<minj;j++){ 
                            C[i][j] += A[i][l] * B[l][j];
                         }
                     }
                 }
             }
         }
     }
 }
*/
/*void matmult_blk(int m,int n,int k,double **A,double **B,double **C, int bs){

    int i,j,l,io,jo,lo;
    int mini, minj, minl;
    init_mat(m,n,C);
    
    
        for(lo=0;lo<k;lo+=bs){
            minl = min(k-lo,bs)+lo;
            for(jo=0;jo<n;jo+=bs){
                minj = min(n-jo,bs)+jo;
                for(io=0;io<m;io+=bs){

                    mini = min(m-io,bs)+io;
                    for(l=lo;l<minl;l++){
                        for(j=jo;j<minj;j++){
                            for(i=io;i<mini;i++){
                        
                            C[i][j] += A[i][l] * B[l][j];
                        }
                    }
                }
            }
        }
    }
}*/
