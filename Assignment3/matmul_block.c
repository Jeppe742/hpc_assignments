__global__ void matmul_block(int m,int n,int k,double *A,double *B,double *C){

    int A_width = k;
    int B_width = n;

    // Block row and column
    int blockRow = blockIdx.y;
    int blockCol = blockIdx.x;

    double *Csub = &C[n*BLOCK_SIZE*blockRow+BLOCK_SIZE*blockCol];

    double cvalue = 0.0;

    int row = threadIdx.y;
    int col = threadIdx.x;

    __shared__ *double Asub[BLOCK_SIZE*BLOCK_SIZE];
    __shared__ *double Bsub[BLOCK_SIZE*BLOCK_SIZE];


    for (int m=0; m < (A_width / BLOCK_SIZE); ++m){

        // Get sub of A and B
        double *Asub = &A[k*BLOCK_SIZE*blockRow + BLOCK_SIZE*m];
        double *Bsub = &B[n*BLOCK_SIZE*m + BLOCK_SIZE*blockCol];

        __synchthreads();

        for (int e=0; e<BLOCK_SIZE; ++e){
            cvalue += Asub[row*A_width + e] * Bsub[e*B_width + col];
        }

    __synchthreads();


    }
    Csub[n * row + col] = cvalue;

}