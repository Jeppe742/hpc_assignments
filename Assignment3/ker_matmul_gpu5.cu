__global__ void ker_matmul_gpu5(int m,int n,int k,double *A,double *B,double *C){
    
int a,b,c;
    /*Block row*/
    int i = blockIdx.y;
    /*Block column*/
    int j = blockIdx.x;
    /*Thread row and column inside C-submatrix*/
    int l = threadIdx.y; 
    int k = threadIdx.x;
    /*Csub=C[n*bs*i+bs*j]*/
   
    double tmp = 0.0;


    for(a=0; a < m/blockDim.y; a++){
        __shared__ double asub[blockDim.x*blockDim.y];
        __shared__ double bsub[blockDim.x*blockDim.y];
        asub=A[k*bs*i+bs*a];
        bsub=B[n*bs*a+j*bs];

        __syncthreads();
        
        for(b=0; b < blockDim.x; b++){

            tmp += asub[l*blockDim.y + b] * bsub[b*blockDim.x + k];

        }
        __syncthreads();
        }
        C[n*blockDim.y*i+blockDim.x*j] = tmp;
}