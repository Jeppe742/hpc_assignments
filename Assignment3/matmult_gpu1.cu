void matmult_gpu1(int m, int n, int k, double *A, double *B, double *C){
    double *A_d, *B_d, *C_d;
    cudaMalloc((void **)&A_d,sizeof(double)*m*k);
    cudaMalloc((void **)&B_d,sizeof(double)*k*n);
    cudaMalloc((void **)&C_d,sizeof(double)*m*n);
    cudaMemcpy(A_d,A,sizeof(double)*m*k,cudaMemcpyHostToDevice);
    cudaMemcpy(B_d,B,sizeof(double)*k*n,cudaMemcpyHostToDevice);
    /*Kernel launch */
    matmult_ker_gpu1<<<1,1>>>(m,n,k, A_d, B_d,C_d);
    cudaDeviceSynchronize();
    cudaMemcpy(C,C_d,sizeof(double)*m*n,cudaMemcpyDeviceToHost);
    cudaFree(A_d);
    cudaFree(B_d);
    cudaFree(C_d);
}