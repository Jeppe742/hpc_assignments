#!/bin/bash
#BSUB -q hpcintrogpu
#BSUB -n 1
#BSUB -W 30
#BSUB -J cuda
#BSUB -gpu "num=1:mode=exclusive_process"
#BSUB -o logs/bench_cuda_mm_%J.out
#BSUB -e logs/bench_cuda_mm_%J.err
#BSUB -N
#BSUB -R "rusage[mem=16GB]"
#BSUB -R "span[hosts=1]"

export MATMULT_COMPARE=0
for n in 2 4 8 16 32 64 128 256 512 1024 2048 3000 4000 6000 8000 10000 15000
do
    for method in gpu2 gpu3 gpu4 gpu5 gpulib
    do
    ./matmult_f.nvcc $method $n $n $n  >> logs/gpu_compare.txt
    done
    # echo -ne "$n\t" >> logs/mem_cpy.txt
    # nvprof ./matmult_f.nvcc gpu2 $n $n $n 2>&1 >/dev/null |grep -- 'CUDA\|matmult_ker'| sort -k8 |xargs -n1 |grep % |xargs -n3  >> logs/mem_cpy.txt   
done
