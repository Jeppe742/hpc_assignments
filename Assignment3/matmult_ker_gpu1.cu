__global__ void matmult_ker_gpu1(int m, int n, int k, double *A,double *B, double *C){
    int i,j,l;
    /*Remember initializing*/
    for(i=0;i<m;i++){
        for(l=0;l<k;l++){
            for(j=0;j<n;j++){
                C[i*n+j] += A[i*k+l] * B[l*n+j];
            }
        }
    }
}
}