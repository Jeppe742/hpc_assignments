void jacobi(double *** u_old, double *** u_new, double *** f, int N, int max_iter, double tol);
__global__ void jacobi_serial_kernel(double *** u_old, double *** u_new, double *** f, double *** tmp_p, double delta, double delta2, double factor, int N);
__global__ void jacobi_naive_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N);
__global__ void jacobi_reduction_kernel(double *** u_old, double *** u_new, double *** f, double delta2, double factor, int N, double *res );
void jacobi_split_gpu(double ***u_old_gpu0, double ***u_old_gpu1, double *** u_new_gpu0,double *** u_new_gpu1, double *** f_gpu0,double *** f_gpu1,int N,int iter_max);
__global__ void jacobi_gpu0_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N, double ***ghost_point);
__global__ void jacobi_gpu1_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N, double ***ghost_point);

__global__ void jacobi_reduction_kernel(double *** u_old, double *** u_new, double *** f, double delta2, double factor, int N, double *res  );
