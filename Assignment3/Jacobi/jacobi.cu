/* jacobi.c - Poisson problem in 3d
 * 
 */
#include <math.h>
#include <stdio.h>
#include <omp.h>
#include "jacobi.h"
#include "transfer3d_gpu.h"
#include <helper_cuda.h>

void jacobi(double *** u_old, double *** u_new, double *** f, int N, int max_iter, double tol){
    double delta = 2.0/(N-1);
    double delta2 = delta*delta;
    int iter = 0;
    double factor= 1.0/6.0;
    double *** tmp_p;
    double * d_res;
    double * h_res;
    double zero=0.0;
    cudaMalloc((void**)&d_res,sizeof(double));
    cudaMallocHost((void**)&h_res,sizeof(double));
    *h_res=10e6;
    while(iter<max_iter && *h_res>tol){
    //jacobi_serial_kernel<<<1,1>>>(u_old,u_new,f,tmp_p,delta,delta2,factor,N);

    //Opgave 6

    dim3 block(10,10,10);
    dim3 grid(ceil((double)N/10.0),ceil((double)N/10.0),ceil((double)N/10.0));
    // jacobi_naive_kernel<<<grid,block>>>(u_old, u_new,f ,delta,delta2, factor, N);
    jacobi_reduction_kernel<<<grid, block>>>(u_old, u_new, f, delta2, factor, N, d_res);
    cudaMemcpy(h_res,d_res,sizeof(double),cudaMemcpyDeviceToHost);
    cudaMemcpy(d_res, &zero, sizeof(double),cudaMemcpyHostToDevice);
    iter ++;
    tmp_p = u_old;
    u_old = u_new;
    u_new = tmp_p;


    }
        cudaFree(d_res);
    cudaFreeHost(h_res);
}

__global__ void jacobi_serial_kernel(double *** u_old, double *** u_new, double *** f, double ***tmp_p, double delta, double delta2, double factor, int N){
    int i,k,j;
    for(i=1;i<(N-1);i++){
        for(j=1;j<(N-1);j++){
            for(k=1;k<(N-1);k++){

                u_new[i][j][k] = factor*( u_old[i-1][j][k] + u_old[i+1][j][k] \
                                        + u_old[i][j-1][k] + u_old[i][j+1][k] \
                                        + u_old[i][j][k-1] + u_old[i][j][k+1] \
                                                        + delta2*f[i][j][k] );


            }
        }
    }

}

__global__ void jacobi_naive_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N){
    int x = threadIdx.x + blockDim.x * blockIdx.x+1;
    int y = threadIdx.y + blockDim.y * blockIdx.y+1;
    int z = threadIdx.z + blockDim.z * blockIdx.z+1;
    if(x < N-1 && y < N-1 && z < N-1){
        u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z] \
            + u_old[x][y-1][z] + u_old[x][y+1][z] \
            + u_old[x][y][z-1] + u_old[x][y][z+1] \
            + delta2*f[x][y][z] );

    }

}
void jacobi_split_gpu(double ***u_old_gpu0, double ***u_old_gpu1, double *** u_new_gpu0,double *** u_new_gpu1, double *** f_gpu0,double *** f_gpu1,int N,int max_iter){
    double delta = 2.0/(N-1);
    double delta2 = delta*delta;
    int iter = 0;
    double factor= 1.0/6.0;
    double *** tmp_p0;
    double *** tmp_p1;
    int i = 0;
    double bs = 10;
    dim3 block(bs,bs,bs);
    dim3 grid(ceil((double)(N)/bs),ceil((double)(N)/bs),ceil((double)(N)/(bs*2.0)));

    while(iter<max_iter){
    cudaSetDevice(0);
    cudaDeviceEnablePeerAccess(1,0);
    //jacobi_gpu0_kernel<<<grid,block>>>(u_old_gpu0,u_new_gpu0,f_gpu0,delta,delta2,factor,N,u_old_gpu1);
    cudaSetDevice(1);
    cudaDeviceEnablePeerAccess(0,0);
    //jacobi_gpu1_kernel<<<grid,block>>>(u_old_gpu1,u_new_gpu1,f_gpu1,delta,delta2,factor,N,u_old_gpu0);
    cudaDeviceSynchronize();
    iter ++;
    cudaSetDevice(0);
    tmp_p0 = u_old_gpu0;
    u_old_gpu0 = u_new_gpu0;
    u_new_gpu0 = tmp_p0;
    cudaSetDevice(1);
    tmp_p1 = u_old_gpu1;
    u_old_gpu1 = u_new_gpu1;
    u_new_gpu1 = tmp_p1;

    }
}

__global__ void jacobi_gpu0_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N, double ***ghost_point){
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    int z = threadIdx.z + blockDim.z * blockIdx.z;
    //printf("x= %d, y = %d\n", x,y);
    if(x < N-1 && y < N-1 && z <= N/2-1){
        if(x==1&&y==1){
            printf("z=%d",z);
        }
        if(z == N/2-1){
            u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z]   \
                + u_old[x][y-1][z] + u_old[x][y+1][z] \
                + u_old[x][y][z-1] + ghost_point[x][y][0] \
                + delta2*f[x][y][z] );
        }
        else{
            u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z]   \
                + u_old[x][y-1][z] + u_old[x][y+1][z] \
                + u_old[x][y][z-1] + u_old[x][y][z+1] \
                + delta2*f[x][y][z] );

        }
    }
}

__global__ void jacobi_gpu1_kernel(double *** u_old, double *** u_new, double *** f, double delta, double delta2, double factor, int N, double ***ghost_point){
    int x = threadIdx.x + blockDim.x * blockIdx.x+1;
    int y = threadIdx.y + blockDim.y * blockIdx.y+1;
    int z = threadIdx.z + blockDim.z * blockIdx.z;
    //printf("x= %d, y = %d\n", x,y);
    if(x < N-1 && y < N-1 && z < N/2){
        if(x==1&&y==1){
            printf("z=%d\n",z);
        }
        if(z == 0){
            u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z]   \
                + u_old[x][y-1][z] + u_old[x][y+1][z] \
                + ghost_point[x][y][N/2-1] + u_old[x][y][z+1] \
                + delta2*f[x][y][z] );
        }
        else{
            u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z]   \
                + u_old[x][y-1][z] + u_old[x][y+1][z] \
                + u_old[x][y][z-1] + u_old[x][y][z+1] \
                + delta2*f[x][y][z] );
        }

    }
}



__inline__ __device__ double warpReduceSum(double value){
    for(int i=16; i>0; i/=2){
        value += __shfl_down_sync(-1,value,i);
    }
    return value;
}
__inline__ __device__ double blockReduceSum(double value){
    __shared__ double smem[32];
    int idx=threadIdx.x+blockDim.x*threadIdx.y+blockDim.y*blockDim.x*threadIdx.z;
    if(idx<32){
        smem[idx]=0;
    }
    __syncthreads();

    value=warpReduceSum(value);
    if(idx%32==0){
        smem[idx/32]=value;
    }
    __syncthreads();
    if(idx<32){
        value=smem[idx];
    }
    return warpReduceSum(value);
}

__global__ void jacobi_reduction_kernel(double *** u_old, double *** u_new, double *** f, double delta2, double factor, int N, double *res){
    double tmp, value;
    int x = threadIdx.x + blockDim.x * blockIdx.x+1;
    int y = threadIdx.y + blockDim.y * blockIdx.y+1;
    int z = threadIdx.z + blockDim.z * blockIdx.z+1;
    int local_idx = threadIdx.x+blockDim.x*threadIdx.y+blockDim.y*blockDim.x*threadIdx.z;
    int block_idx = blockIdx.x+ blockIdx.y * gridDim.x+ gridDim.x * gridDim.y * blockIdx.z;
    int global_idx = block_idx * (blockDim.x * blockDim.y * blockDim.z)+ (threadIdx.z * (blockDim.x * blockDim.y))+ (threadIdx.y * blockDim.x)+ threadIdx.x;
    
    if(x < N-1 && y < N-1 && z < N-1){

        tmp = u_old[x][y][z];
        u_new[x][y][z] = factor*(u_old[x-1][y][z] + u_old[x+1][y][z] \
            + u_old[x][y-1][z] + u_old[x][y+1][z] \
            + u_old[x][y][z-1] + u_old[x][y][z+1] \
            + delta2*f[x][y][z] );

        value = (u_new[x][y][z] - tmp)*(u_new[x][y][z] - tmp);

        value = blockReduceSum(value);
        if(local_idx==0 ){
            atomicAdd(res, value);
        }
        __syncthreads();
    }

}


