/* main.c - Poisson problem in 3D
 *
 */


#include <stdlib.h>
#include "jacobi.h"
#include "transfer3d_gpu.h"
#include "alloc3d_gpu.h"
extern "C"{
#include "print.h"
#include "alloc3d.h"
#include "init.h"
}

#define N_DEFAULT 30


#include <stdio.h>
int
main(int argc, char *argv[]) {
    
    int 	N = N_DEFAULT;
    int 	iter_max = 1000;
    double	tolerance;
    double	start_T;
    int		output_type = 4;
    char	*output_prefix = "poisson_jacobi";
    char        *output_ext    = "";
    char	output_filename[FILENAME_MAX];
    double 	***u = NULL;
    double 	***u2 = NULL;
    double  ***f = NULL;
    double t1, t2;


   //get the paramters from the command line 
    N         = atoi(argv[1]) + 2;	// grid size
    
    iter_max  = atoi(argv[2]);  // max. no. of iterations
    tolerance = atof(argv[3]);  // tolerance
    start_T   = atof(argv[4]);  // start T for all inner grid points
    if (argc == 6) {
	output_type = atoi(argv[5]);  // ouput type
    }
    // allocate memory
    bool onegpu = 0;
    
    u = d_malloc_3d(N, N, N);
    u2 = d_malloc_3d(N,N,N);
    f = d_malloc_3d(N, N, N);
    init_problem(u, u2, f, N, start_T);
    /*
    double *** u_gpu = d_malloc_3d_gpu(N,N,N);
    double *** u2_gpu = d_malloc_3d_gpu(N,N,N);
    double *** f_gpu = d_malloc_3d_gpu(N,N,N);
    */

    cudaSetDevice(0);
    double *** u_gpu0 = d_malloc_3d_gpu(N,N,N/2);
    double *** u2_gpu0 = d_malloc_3d_gpu(N,N,N/2);
    double *** f_gpu0 = d_malloc_3d_gpu(N,N,N/2);
    cudaSetDevice(1); 
    double *** u_gpu1 = d_malloc_3d_gpu(N,N,N/2);
    double *** u2_gpu1 = d_malloc_3d_gpu(N,N,N/2);
    double *** f_gpu1 = d_malloc_3d_gpu(N,N,N/2);
    
    if (u == NULL || f==NULL) {
        perror("array u: allocation failed");
        exit(-1);
    }

    cudaSetDevice(0);
    transfer_3d_from_1d(u_gpu0,u[0][0],N,N,N/2,1);
    transfer_3d_from_1d(u2_gpu0,u2[0][0],N,N,N/2,1);
    transfer_3d_from_1d(f_gpu0,f[0][0],N,N,N/2,1);
    cudaSetDevice(1);
    transfer_3d_from_1d(u_gpu1,u[0][0]+N*N*N/2,N,N,N/2,1);
    transfer_3d_from_1d(u2_gpu1,u2[0][0]+N*N*N/2,N,N,N/2,1);
    transfer_3d_from_1d(f_gpu1,f[0][0]+N*N*N/2,N,N,N/2,1);



    //jacobi(u_gpu, u2_gpu, f_gpu, N, iter_max);
    jacobi_split_gpu(u_gpu0,u_gpu1,u2_gpu0,u2_gpu1,f_gpu0,f_gpu1,N,iter_max);
    double *** u0_host = d_malloc_3d(N,N,N/2);
    double *** u1_host = d_malloc_3d(N,N,N/2);
    double *** u_new = d_malloc_3d(N,N,N);
    /*
    cudaSetDevice(0);
    transfer_3d_to_1d(u_new[0][0],u2_gpu0,N,N,N/2,2);
    cudaSetDevice(1);
    transfer_3d_to_1d(u_new[0][0]+N*N*N/2,u2_gpu1,N,N,N/2,2);
*/
    // dump  results if wanted 
    switch(output_type) {
	case 0:
	    // no output at all
	    break;
    case 1:
        // used to print convergence from functions
        break;

    case 2:
    /*
        // print N, total cpu time, and time per iterations 
        #ifdef _JACOBI
        printf(" j %d %d %f %f %f\n",omp_get_max_threads() ,N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #elif _JACOBI_PAR
        printf(" first_touch %d %d %f %f %f\n",omp_get_max_threads(), N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #else
        printf("gs %d %d %f %f %f\n",omp_get_max_threads(), N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #endif
        break;
        */
	case 3:
	    output_ext = ".bin";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write binary dump to %s: \n", output_filename);
	    print_binary(output_filename, N, u);
	    break;
    case 4:
	    output_ext = ".vtk";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write VTK file to %s: \n", output_filename);
	    print_vtk(output_filename, N, u_new);
        break;
	default:
	    fprintf(stderr, "Non-supported output type!\n");
	    break;
    }

    // de-allocate memory
    free(u);
    free(u2);
    free(f);
    free(u0_host);
    free(u1_host);
    free(u_new);
    cudaSetDevice(0);
    free_gpu(u_gpu0);
    free_gpu(u2_gpu0);
    free_gpu(f_gpu0);
    cudaSetDevice(1);
    free_gpu(u_gpu1);
    free_gpu(u2_gpu1);
    free_gpu(f_gpu1);

    return(0);
}
