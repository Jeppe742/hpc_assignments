#include <stdio.h>
__inline__ __device__ double warpReduceSum(double value){
    for(int i=16; i>0; i/=2){
        value += __shfl_down_sync(-1,value,i);
    }
    return value;
}
__inline__ __device__ double blockReduceSum(double value){
    __shared__ double smem[32];
    int idx=threadIdx.x+blockDim.x*threadIdx.y+blockDim.y*blockDim.x*threadIdx.z;
    if(idx<32){
        smem[idx]=0;
    }
    __syncthreads();

    value=warpReduceSum(value);
    if(idx%32==0){
        smem[idx/32]=value;
    }
    __syncthreads();
    if(idx<32){
        value=smem[idx];
    }
    return warpReduceSum(value);
}

__global__ __device__ void reduction(double *a, int n, double *res){
    int local_idx = threadIdx.x+blockDim.x*threadIdx.y+blockDim.y*blockDim.x*threadIdx.z;
    int block_idx = blockIdx.x+ blockIdx.y * gridDim.x+ gridDim.x * gridDim.y * blockIdx.z;
    int global_idx = block_idx * (blockDim.x * blockDim.y * blockDim.z)+ (threadIdx.z * (blockDim.x * blockDim.y))+ (threadIdx.y * blockDim.x)+ threadIdx.x;
    
    double value = global_idx<n?a[global_idx]:0;

    value = blockReduceSum(value);
    if(local_idx==0 ){
        atomicAdd(res, value);
    }
    __syncthreads();
    if(global_idx==0){
        printf("val=%f\n",*res);
    }
}

int main(int argc, char *argv[]){

    double h_list [100] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99};
    double * h_res;
    double * d_res;
    double * d_list ;

    cudaMalloc((void**)&d_list,100*sizeof(double));
    cudaMalloc((void**)&d_res,sizeof(double));
    cudaMemcpy(d_list,h_list,100*sizeof(double),cudaMemcpyHostToDevice);
    cudaMemcpy(d_res,h_res,sizeof(double),cudaMemcpyHostToDevice);

    dim3 DimBlock(2,2,2);
    dim3 DimGrid(100/8+1,100/8+1,100/8+1);
    reduction<<<DimGrid, DimBlock>>>(d_list,100,d_res);
    cudaDeviceSynchronize();
    // free(h_list);
    // free(h_res);
    cudaFree(d_list);
    cudaFree(d_res);
    return 1;
}