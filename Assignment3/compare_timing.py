import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 30})

df = pd.read_csv("logs/gpu_compare.txt", header=None, sep=r"\s+")

df.columns=["memory","mflops","","method"]
df['mflops'] = df['mflops']/1024
df.groupby(["memory","method"]).mean()["mflops"].unstack().plot(markersize=20,style='.-')
plt.xscale('log')
plt.ylabel('GFlop/s')
plt.xlabel('Memory size [kB]')


pct2float = lambda x : float(x.strip('%'))

df = pd.read_csv("logs/mem_cpy.txt", header=None, sep=r"\s+", converters={1:pct2float, 2:pct2float, 3:pct2float})

df.columns=["N","kernel","H2D","D2H"]
df = df.set_index('N').stack().reset_index()
df.columns=['N','Function','percent']

df.groupby(["N","Function"]).mean()["percent"].unstack().plot.area()
# plt.xscale('log')
plt.ylabel(r'% of runtime')
plt.xlabel('N')



plt.show()