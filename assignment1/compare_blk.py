import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})

df = pd.read_csv("logs/compare_blk.txt", header=None, sep=r"\s+")

df.columns=["mem","mflops","#","function","Block size"]
df["function"] = df["function"].str.replace('matmult_','')
df.groupby(["mem","Block size"]).mean()["mflops"].unstack().plot(markersize=20,style='.-')
plt.xlabel('Memory size [kB]')
plt.ylabel('Performance [MFlops]')
plt.xscale('log')
plt.title("Performance of block size as function of problem size")
xmin, xmax, ymin, ymax = plt.axis()
plt.xlim([xmin, xmax])
plt.ylim([ymin, ymax])
plt.plot([32,32],[ymin, ymax],'k--')
plt.plot([256,256],[ymin, ymax],'k--')
plt.plot([30720,30720],[ymin, ymax],'k--')
plt.text(9,0,'L1-cache')
plt.text(72,0,'L2-cache')
plt.text(9000,0,'L3-cache')
plt.show()