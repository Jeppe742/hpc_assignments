#include "cblas.h"

#define min(a,b) ( (a) < (b) ? (a) : (b))
void init_mat(int m, int n, double **C){
    int i, j;
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            C[i][j] = 0.0;
        }
    }
}

void matmult_nat(int m,int n,int k,double **A,double **B,double **C){
    int i,j,l;
    init_mat(m,n,C);

    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            for(l=0;l<k;l++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}

void matmult_lib(int m,int n,int k,double **A,double **B,double **C){

    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,m,n,k,1.0,A[0], k,B[0],n,0,C[0],n);
}
void matmult_mnk(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            for(l=0;l<k;l++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
void matmult_mkn(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(i=0;i<m;i++){
        for(l=0;l<k;l++){
            for(j=0;j<n;j++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
void matmult_kmn(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(l=0;l<k;l++){
        for(i=0;i<m;i++){
            for(j=0;j<n;j++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
void matmult_knm(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(l=0;l<k;l++){
        for(j=0;j<n;j++){
            for(i=0;i<m;i++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
void matmult_nkm(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(j=0;j<n;j++){
        for(l=0;l<k;l++){
            for(i=0;i<m;i++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
void matmult_nmk(int m,int n,int k,double **A,double **B,double **C){    
    int i,j,l;
    init_mat(m,n,C);
    for(j=0;j<n;j++){
        for(i=0;i<m;i++){
            for(l=0;l<k;l++){
                C[i][j] += A[i][l] * B[l][j];
            }
        }
    }
}
// void matmult_blk(int m,int n,int k,double **A,double **B,double **C, int bs){

//     int i,j,l,io,jo,lo;
//     int mini, minj, minl;
//     init_mat(m,n,C);
    
//     for(io=0;io<m;io+=bs){
//         mini = min(m-io,bs)+io;
//         for(lo=0;lo<k;lo+=bs){
//             minl = min(k-lo,bs)+lo;
//             for(jo=0;jo<n;jo+=bs){
//                 minj = min(n-jo,bs)+jo;
     
//                 for(i=io;i<mini;i++){
//                     for(l=lo;l<minl;l++){
//                         for(j=jo;j<minj;j++){
                            
                        
//                             C[i][j] += A[i][l] * B[l][j];
//                         }
//                     }
//                 }
//             }
//         }
//     }
// }

void matmult_blk(int m,int n,int k,double **A,double **B,double **C, int bs){

    int i,j,l,io,jo,lo;
    int mini, minj, minl;
    init_mat(m,n,C);
    
    
        for(lo=0;lo<k;lo+=bs){
            minl = min(k-lo,bs)+lo;
            for(jo=0;jo<n;jo+=bs){
                minj = min(n-jo,bs)+jo;
                for(io=0;io<m;io+=bs){

                    mini = min(m-io,bs)+io;
                    for(l=lo;l<minl;l++){
                        for(j=jo;j<minj;j++){
                            for(i=io;i<mini;i++){
                        
                            C[i][j] += A[i][l] * B[l][j];
                        }
                    }
                }
            }
        }
    }
}