#!/bin/bash
# 02614 - High-Performance Computing, January 2018
# 
# batch script to run collect on a decidated server in the hpcintro
# queue
#
# Author: Bernd Dammann <bd@cc.dtu.dk>
#
#BSUB -J compare_serial
#BSUB -o logs/compare_serial_%J.out
#BSUB -q hpcintro
#BSUB -n 1
#BSUB -R "rusage[mem=2048]"
#BSUB -W 5

MAX_ITER=10000
TOL=0.1
START_T=0
# experiment name 
#
JID=${LSB_JOBID}
EXPOUT="logs/$LSB_JOBNAME.${JID}.er"

for N in 5 10 20 30 40 50 60 70 80 90 100; do
    ./poisson_j $N $MAX_ITER $TOL $START_T >>logs/convergence.txt
    ./poisson_gs $N $MAX_ITER $TOL $START_T >>logs/convergence.txt
done