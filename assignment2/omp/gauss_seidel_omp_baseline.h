/* gauss_seidel_omp_baseline.h - Poisson problem
 *
 */
#ifndef _GAUSS_SEIDEL_OMP_BASELINE_H
#define _GAUSS_SEIDEL_OMP_BASELINE_H

int gauss_seidel_omp_baseline(double *** u, double *** f, int N, int max_iter, double  tol);

#endif
