/* main.c - Poisson problem in 3D
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "alloc3d.h"
#include "print.h"

#ifdef _JACOBI_OMP_BASELINE
#include "jacobi_omp_baseline.h"
#endif

#ifdef _GAUSS_SEIDEL_OMP_BASELINE
#include "gauss_seidel_omp_baseline.h"
#endif

#define N_DEFAULT 100

#include "init.h"
#include <omp.h>

int
main(int argc, char *argv[]) {

    int 	N = N_DEFAULT;
    int 	iter_max = 1000;
    double	tolerance;
    double	start_T;
    int		output_type = 0;
    char	*output_prefix = "poisson_jacobi";
    char        *output_ext    = "";
    char	output_filename[FILENAME_MAX];
    double 	***u = NULL;
    double 	***u2 = NULL;
    double  ***f = NULL;
    double t1, t2;


    /* get the paramters from the command line */
    N         = atoi(argv[1]) + 2;	// grid size
    iter_max  = atoi(argv[2]);  // max. no. of iterations
    tolerance = atof(argv[3]);  // tolerance
    start_T   = atof(argv[4]);  // start T for all inner grid points
    if (argc == 6) {
	output_type = atoi(argv[5]);  // ouput type
    }

    // allocate memory
    
    u = d_malloc_3d(N, N, N);
    f = d_malloc_3d(N, N, N);

    #ifdef _JACOBI
    u2 = d_malloc_3d(N, N, N);
    #endif
    
    if (u == NULL || f==NULL) {
        perror("array u: allocation failed");
        exit(-1);
    }
    init_problem(u, u2, f, N, start_T);
    //#ifdef _JACOBI
    //jacobi(u, u2, f, N, iter_max, tolerance);
    //#endif
    t1 = omp_get_wtime();
    #ifdef _GAUSS_SEIDEL_OMP_BASELINE
    gauss_seidel_omp_baseline(u, f, N, iter_max, tolerance);
    #endif
    t2 = omp_get_wtime();

    printf("walltime=%f\n",t2-t1);
    // dump  results if wanted 
    switch(output_type) {
	case 0:
	    // no output at all
	    break;
	case 3:
	    output_ext = ".bin";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write binary dump to %s: ", output_filename);
	    print_binary(output_filename, N, u);
	    break;
	case 4:
	    output_ext = ".vtk";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write VTK file to %s: ", output_filename);
	    print_vtk(output_filename, N, u);
	    break;
	default:
	    fprintf(stderr, "Non-supported output type!\n");
	    break;
    }

    // de-allocate memory
    free(u);

    return(0);
}
