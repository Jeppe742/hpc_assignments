#include <stdio.h>
void init_problem(double *** u, double *** u2, double *** f, int N, double T){
int i, j, k;
double x,y,z;
double bx1, bx2, by1, by2, bz1, bz2;
double delta = 2.0/(N-1);
bx1 = -1.0;
bx2 = -3.0/8.0;
by1 = -1.0;
by2 = -1.0/2.0;
bz1 = -2.0/3.0;
bz2 =  0.0;
#pragma omp parallel for private(i,j,k,x,y,z)
for(i=0;i<N;i++){
    x = -1.0 + i*delta;
    for(j=0;j<N;j++){
        y = -1.0 + j*delta;
        for(k=0;k<N;k++){
            z = -1.0 + k*delta;   

            if( i==0 || i==(N-1)  || j==(N-1) || k==0 || k==(N-1) ){
                u[i][j][k] = 20.0;
                if(u2 != NULL){    
                u2[i][j][k] = 20.0;    
                }

            }
            else if (j==0)
            {
                u[i][j][k] = 0.0;
                if(u2 != NULL){    
                u2[i][j][k] = 0.0;    
                }
            }
            
            else{
                u[i][j][k] = T;
                if(u2 != NULL){ 
                u2[i][j][k] = T; 
                }   
            }


            if( bx1<=x && x<=bx2 && by1<=y && y<=by2 && bz1<=z && z<=bz2){

                f[i][j][k] = 200;
            }
            else{
                f[i][j][k] = 0.0;
            }

        }   

    }   
}

}
