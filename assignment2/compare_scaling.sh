#!/bin/bash
#BSUB -q hpcintro
#BSUB -n 24
#BSUB -W 60
#BSUB -J bench_poisson
#BSUB -o logs/bench_poisson_%J.out
#BSUB -e logs/bench_poisson_%J.err
#BSUB -N
#BSUB -R "rusage[mem=4GB]"
#BSUB -R "span[hosts=1]"
module load clang/9.0.0
THREADS="1 2 4 8 12 16 20 24"
CMD=poisson_gs_par
N=100
IT=2000
TOL=0.05
TS=15

for n in 2 4 8 12 16 20 28 38 46 56 74 80 90 100 110 120 140 160 180 200
do
    for t in $THREADS
    do
        OMP_NUM_THREADS=$t ./$CMD $n $IT $TOL $TS 2 >> logs/gs_scaling_baseline.txt
    done
done