/* gauss_seidel_omp_baseline.c - Poisson problem in 3d
 *
 */
#include <math.h>
#include <stdio.h>
#include <omp.h>

void
gauss_seidel(double *** u, double *** f, int N, int max_iter, double  tol) {

double delta = 2.0/(N-1);
double delta2 = delta*delta;
int i, j, k;
int iter = 0;
double diff = 1000000;
double factor= 1.0/6.0;
double tmp;

while(iter<max_iter){    
    #pragma omp parallel shared(diff) private(i,j,k, tmp)
    {
    #pragma omp for ordered(2)
    for(i=1;i<(N-1);i++){
        for(j=1;j<(N-1);j++){
            #pragma omp ordered depend(sink:i-1,j)  depend(sink:i,j-1) 
            for(k=1;k<(N-1);k++){
                tmp = u[i][j][k];
                u[i][j][k] = factor*( u[i-1][j][k] + u[i+1][j][k] \
                                        + u[i][j-1][k] + u[i][j+1][k] \
                                        + u[i][j][k-1] + u[i][j][k+1] \
                                        + delta2*f[i][j][k] );
                
            
            }
            #pragma omp ordered depend(source)
        }
    } // End of for-loop
    }
    iter ++;
    

} // End of parallel region
// if(iter==max_iter){
//     printf("\n### Reach maximum number of iterations without converging. diff=%f ###\n",diff);
// }
// else if (diff<tol)
// {
//     printf("converged in %d iterations",iter);
// }

}
