import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})

df = pd.read_csv("logs/convergence.txt", header=None, sep=r"\s+")

df.columns=["status","iterations","N","algorithm"]

df.groupby(["N","algorithm"]).mean()["iterations"].unstack().plot(markersize=20,style='.-')
plt.ylabel('Iterations')
plt.xlabel('N')

df = pd.read_csv("logs/convergence2.txt", header=None, sep=r"\s+")

df.columns=["algorithm","difference","iteration"]

df.groupby(["iteration","algorithm"]).mean()["difference"].unstack().plot(markersize=20,style='.-')
plt.ylabel('$\|\|u_{new} - u_{old}\|\|_2$')
plt.xlabel('Iteration')
plt.yscale('log')

plt.show()