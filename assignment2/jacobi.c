/* jacobi.c - Poisson problem in 3d
 * 
 */
#include <math.h>
#include <stdio.h>
#include <omp.h>

double
frobenius_norm(double ***a, double *** b, int N){
    int i, j, k;
    double diff = 0.0;
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            for(k=0;k<N;k++){
                diff += (a[i][j][k]-b[i][j][k])*(a[i][j][k]-b[i][j][k]);
            }
        }
    }
    diff = sqrt(diff);
    return diff;
}


void
jacobi(double *** u_old, double *** u_new, double *** f, int N, int max_iter, double  tol, int output_type){


double delta = 2.0/(N-1);
double delta2 = delta*delta;
int i, j, k;
int iter = 0;
double diff = 1000000;
double factor= 1.0/6.0;
double *** tmp_p;
double tmp;


while(diff>tol && iter<max_iter){
    diff = 0.0;
    

    for(i=1;i<(N-1);i++){
        for(j=1;j<(N-1);j++){
            for(k=1;k<(N-1);k++){

                tmp = u_old[i][j][k];

                u_new[i][j][k] = factor*( u_old[i-1][j][k] + u_old[i+1][j][k] \
                                        + u_old[i][j-1][k] + u_old[i][j+1][k] \
                                        + u_old[i][j][k-1] + u_old[i][j][k+1] \
                                        + delta2*f[i][j][k] );

                diff += (u_new[i][j][k] - tmp)*(u_new[i][j][k] - tmp);

            }
        }
    }

    diff = sqrt(diff);
    iter ++;
    tmp_p = u_old;
    u_old = u_new;
    u_new = tmp_p;
    // printf("j %f %d\n",diff, iter);
}
if(output_type==1){
    if(iter==max_iter){
        printf("notconverged %f %d j\n", diff, N-2);
    }
    else if (diff<=tol)
    {
        printf("converged %d %d j \n",iter, N-2);
    }
}


}

