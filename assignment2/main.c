/* main.c - Poisson problem in 3D
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "alloc3d.h"
#include "print.h"

#ifdef _JACOBI
#include "jacobi.h"
#elif _JACOBI_PAR
#include "jacobi_par.h"
#elif _GAUSS_SEIDEL
#include "gauss_seidel.h"
#elif _GAUSS_SEIDEL_PAR
#include "gauss_seidel_par.h"
#endif


#define N_DEFAULT 100

#include "init.h"
#include "omp.h"

int
main(int argc, char *argv[]) {

    int 	N = N_DEFAULT;
    int 	iter_max = 1000;
    double	tolerance;
    double	start_T;
    int		output_type = 0;
    char	*output_prefix = "poisson_jacobi";
    char        *output_ext    = "";
    char	output_filename[FILENAME_MAX];
    double 	***u = NULL;
    double 	***u2 = NULL;
    double  ***f = NULL;
    double t1, t2;


    /* get the paramters from the command line */
    N         = atoi(argv[1]) + 2;	// grid size
    iter_max  = atoi(argv[2]);  // max. no. of iterations
    tolerance = atof(argv[3]);  // tolerance
    start_T   = atof(argv[4]);  // start T for all inner grid points
    if (argc == 6) {
	output_type = atoi(argv[5]);  // ouput type
    }

    // allocate memory
    
    u = d_malloc_3d(N, N, N);
    f = d_malloc_3d(N, N, N);

    #ifdef _JACOBI
    u2 = d_malloc_3d(N, N, N);
    #elif _JACOBI_PAR
    u2 = d_malloc_3d(N, N, N);
    #endif
    
    if (u == NULL || f==NULL) {
        perror("array u: allocation failed");
        exit(-1);
    }
    init_problem(u, u2, f, N, start_T);
    t1 = omp_get_wtime();
    #ifdef _GAUSS_SEIDEL
    gauss_seidel(u, f, N, iter_max, tolerance, output_type);
    #elif _GAUSS_SEIDEL_PAR
    gauss_seidel(u, f, N, iter_max, tolerance, output_type);
    #else
    jacobi(u, u2, f, N, iter_max, tolerance, output_type);
    #endif

    t2 = omp_get_wtime();


    // dump  results if wanted 
    switch(output_type) {
	case 0:
	    // no output at all
	    break;
    case 1:
        // used to print convergence from functions
        break;
    case 2:
        // print N, total cpu time, and time per iterations 
        #ifdef _JACOBI
        printf(" j %d %d %f %f %f\n",omp_get_max_threads() ,N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #elif _JACOBI_PAR
        printf(" first_touch %d %d %f %f %f\n",omp_get_max_threads(), N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #else
        printf("gs %d %d %f %f %f\n",omp_get_max_threads(), N-2, t2-t1, iter_max/(t2-t1),iter_max/(t2-t1)*(N-2)*(N-2)*(N-2));
        #endif
        break;
	case 3:
	    output_ext = ".bin";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write binary dump to %s: \n", output_filename);
	    print_binary(output_filename, N, u);
	    break;
	case 4:
	    output_ext = ".vtk";
	    sprintf(output_filename, "%s_%d%s", output_prefix, N, output_ext);
	    fprintf(stderr, "Write VTK file to %s: \n", output_filename);
	    print_vtk(output_filename, N, u);
	    break;
	default:
	    fprintf(stderr, "Non-supported output type!\n");
	    break;
    }

    // de-allocate memory
    free(u);
    free(u2);
    free(f);

    return(0);
}
