import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 30})

df = pd.read_csv("logs/iter_pr_s.txt", header=None, sep=r"\s+")

df.columns=["algorithm","N","time","iter_pr_s","lsu_pr_s"]

df.groupby(["N","algorithm"]).mean()["iter_pr_s"].unstack().plot(markersize=20,style='.-')
plt.ylabel('Iterations per second')
plt.xlabel('N')

df.groupby(["N","algorithm"]).mean()["lsu_pr_s"].unstack().plot(markersize=20,style='.-')
plt.ylabel('Latice site updates per second')
plt.xlabel('N')
# df = pd.read_csv("logs/convergence2.txt", header=None, sep=r"\s+")

# df.columns=["algorithm","difference","iteration"]

# df.groupby(["iteration","algorithm"]).mean()["difference"].unstack().plot(markersize=20,style='.-')
# plt.ylabel('$\|\|u_{new} - u_{old}\|\|_2$')
# plt.xlabel('Iteration')
plt.yscale('log')

plt.show()