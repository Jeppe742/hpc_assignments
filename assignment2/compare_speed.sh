#!/bin/bash
# 02614 - High-Performance Computing, January 2018
# 
# batch script to run collect on a decidated server in the hpcintro
# queue
#
# Author: Bernd Dammann <bd@cc.dtu.dk>
#
#BSUB -J compare_serial
#BSUB -o logs/compare_serial_%J.out
#BSUB -q hpcintro
#BSUB -n 1
#BSUB -R "rusage[mem=2048]"
#BSUB -W 5

MAX_ITER=1000000
TOL=-1
START_T=0
# experiment name 
#
JID=${LSB_JOBID}
EXPOUT="logs/$LSB_JOBNAME.${JID}.er"

for N in 5 10 20 30 40 50 60 70 80 90 100 100 120; do
    if [ $N -eq 10 ] 
    then
        MAX_ITER=100000
    elif [ $N -gt 30 ]&&[ $N -lt 70 ]
    then    
        MAX_ITER=10000
    elif [ $N -gt 60 ]
    then 
        MAX_ITER=1000
    fi
    ./poisson_j $N $MAX_ITER $TOL $START_T 2 >>logs/iter_pr_s.txt
    ./poisson_gs $N $MAX_ITER $TOL $START_T 2 >>logs/iter_pr_s.txt
done