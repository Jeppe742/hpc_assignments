/* gauss_seidel.c - Poisson problem in 3d
 *
 */
#include <math.h>
#include <stdio.h>

void
gauss_seidel(double *** u, double *** f, int N, int max_iter, double  tol, int output_type) {

double delta = 2.0/(N-1);
double delta2 = delta*delta;
int i, j, k;
int iter = 0;
double diff = 1000000;
double factor= 1.0/6.0;
double tmp;

while(diff>tol && iter<max_iter){
    diff = 0.0;
    for(i=1;i<(N-1);i++){
        for(j=1;j<(N-1);j++){
            for(k=1;k<(N-1);k++){
                tmp = u[i][j][k];
                u[i][j][k] = factor*( u[i-1][j][k] + u[i+1][j][k] \
                                    + u[i][j-1][k] + u[i][j+1][k] \
                                    + u[i][j][k-1] + u[i][j][k+1] \
                                    + delta2*f[i][j][k] );
                diff += (u[i][j][k] - tmp)*(u[i][j][k] - tmp);
            }
        }
    }
    diff = sqrt(diff);

    iter ++;

    // printf("gs %f %d\n",diff, iter);
}
if(output_type==1){
    if(iter==max_iter){
        printf("notconverged %f %d gs\n", diff, N-2);
    }
    else if (diff<=tol)
    {
        printf("converged %d %d gs \n",iter, N-2);
    }
}
}

