import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 20})

df_baseline = pd.read_csv("logs/jacobi_scaling_baseline.txt", header=None, sep=r"\s+")
df_ft = pd.read_csv("logs/jacobi_scaling_noopt.txt", header=None, sep=r"\s+")

df_baseline.columns=["algorithm","threads","N","time","iter_pr_s","lsu_pr_s"]
df_ft.columns=["algorithm","threads","N","time","iter_pr_s","lsu_pr_s"]

for df, name in zip([df_baseline, df_ft],['-O3','N/A']):
    df.groupby(["N","threads"]).mean()["lsu_pr_s"].unstack().plot(markersize=10,style='.-')
    xmin, xmax, ymin, ymax = plt.axis()
    plt.xlim([xmin, xmax])
    plt.ylim([ymin, ymax])
    plt.plot([140,140],[ymin, ymax],'k--')
    plt.text(125,0,'L3-cache')
    plt.ylabel('Lattice site update per second')
    plt.xlabel('N')
    plt.title(name)


plt.figure()
for i,n in enumerate([20, 56, 100, 200]):
    plt.subplot(2,2,i+1)
    for df,label, marker in zip([df_baseline[df_baseline.N==n], df_ft[df_ft.N==n]],['-O3','N/A'],['.','s']):
        df = df.copy()
        df['speedup'] = df['time'].iloc[0]/df['time']
        df['efficiency'] = df['speedup']/df['threads']
        plt.plot(df['threads'],df['speedup'], label=label, marker=marker)
    plt.plot(df['threads'],df['threads'], label='ideal scaling')
    plt.ylabel('Speedup')
    plt.xlabel('Threads')
    plt.legend()
    plt.title(f'N={n}')

# plt.figure()
# for i,n in enumerate([ 100, 200]):
#     plt.subplot(1,2,i+1)
#     for df,label, marker in zip([df_sockets_close[df_sockets_close.N==n], df_sockets_spread[df_sockets_spread.N==n], df_cores_close[df_cores_close.N==n], df_cores_spread[df_cores_spread.N==n]],['sockets + close',' socket + spread',' cores + close','cores + spread'],['.','*','D','s']):
#         df = df.copy()
#         df['speedup'] = df['time'].iloc[0]/df['time']
#         df['efficiency'] = df['speedup']/df['threads']
#         plt.plot(df['threads'],df['speedup'], label=label, marker=marker)
#     plt.plot(df['threads'],df['threads'], label='ideal scaling')
#     plt.ylabel('Speedup')
#     plt.xlabel('Threads')
#     plt.legend()
#     plt.title(f'N={n}')


# plt.figure()
# df = df_gs_ft[df_gs_ft.N==100].copy()
# df['speedup'] = df['time'].iloc[0]/df['time']
# plt.plot(df['threads'],df['speedup'], label='Gauss', marker='.')
# plt.plot(df['threads'],df['threads'],label='ideal scaling')
# plt.ylabel('speedup')
# plt.xlabel('threads')
# plt.legend()

# plt.plot(df['threads'],df['time'])
# plt.ylabel('Runtime')
# plt.xlabel('Threads')

# plt.figure()
# plt.plot(df['threads'],df_baseline['speedup'], label='baseline')
# plt.plot(df['threads'],df_ft['speedup'], label='first touch')
# plt.plot(df['threads'],df_close['speedup'], label='first touch + close')
# plt.plot(df['threads'],df_spread['speedup'], label='first touch + spread')
# plt.plot(df['threads'],df['threads'], label='ideal scaling')

# plt.plot(df['threads'],df['threads'], label='ideal scaling')
# plt.ylabel('Speedup')
# plt.xlabel('Threads')
# plt.legend()
# plt.figure()
# plt.plot(df['threads'],df['efficiency'])
# plt.ylabel('Efficiency')
# plt.xlabel('Threads')



plt.show()